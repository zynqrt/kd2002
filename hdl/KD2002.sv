`include "typedef.svh" 

module KD2002 #
(
	parameter integer SHIFTREG_1_SIZE = 192,
	parameter integer SHIFTREG_2_SIZE = 256
)
( 
	input wire aclk,
	input wire aresetn,

	// Логическая 1 сбрасывает счетчик 
	input wire init,
	input wire print_line,
	output wire data_loaded, // Данные загружены в голову

	// Выходы на саму голову
	output wire HEAD_CLK,
	input wire [1:0] HEAD_DI,
	output wire [1:0] HEAD_DO,
	output wire [1:0] HEAD_STB,
	output wire HEAD_LATCHn,

	BRAM_if.master bram
);
	
localparam SHIFTREG_1_SIZE_BYTES = SHIFTREG_1_SIZE / 8;
localparam SHIFTREG_2_SIZE_BYTES = SHIFTREG_2_SIZE / 8;

	assign bram.rst = ~aresetn;
	assign bram.dout = '0;
	assign bram.we = 0;
	assign bram.en = '1;
	

bit [15:0] bram_address;
bit bram_address_inc;

bit fifo_reset;
bit [7:0] dummy_counter;
bit [7:0] valid_counter;
bit [1:0] latch_clk;

logic fifo_stb1_empty, fifo_stb2_empty;
logic fifo_stb1_empty_delayed, fifo_stb2_empty_delayed;
	DelayLine #(.TAPS(3)) dl_empty1_u(
		.aclk      (aclk),
		.aresetn   (aresetn),
		.signal_in (fifo_stb1_empty),
		.signal_out(fifo_stb1_empty_delayed));
	
	DelayLine #(.TAPS(3)) dl_empty2_u(
		.aclk      (aclk),
		.aresetn   (aresetn),
		.signal_in (fifo_stb2_empty),
		.signal_out(fifo_stb2_empty_delayed));

enum bit [31:0] {
	IDLE,
	INIT,
	LOAD_STB1_DUMMY, // Короткий регистр, грузим пустые биты
	LOAD_STB1_VALID, // Короткий регистр, грузим реальные данные
	END_OF_LOAD_STB1,
	LOAD_STB2_VALID, // Длинный регистр
	END_OF_LOAD_STB2,
	LOAD_TO_HEAD,
	DELAY_TO_WAIT_PRINT_LINE,
	WAIT_PRINT_LINE,  // Данные загружены, ожидаем начало записи
	LATCH_DELAY,
	LATCH,
	
	PRINTING
} state, next_state;
	
	always_ff @(posedge aclk)
		if (aresetn == '0)
			state <= IDLE;
		else
			state <= next_state;

	always_comb begin : state_logic
		next_state = state;
		unique case(state)
			IDLE: if (init) next_state = INIT;
			INIT: next_state = LOAD_STB1_DUMMY;
			LOAD_STB1_DUMMY: if (dummy_counter == (SHIFTREG_2_SIZE_BYTES - SHIFTREG_1_SIZE_BYTES)) next_state = LOAD_STB1_VALID;
			LOAD_STB1_VALID: if (valid_counter == SHIFTREG_1_SIZE_BYTES) next_state = END_OF_LOAD_STB1;
			END_OF_LOAD_STB1: next_state = LOAD_STB2_VALID;
			LOAD_STB2_VALID: if (valid_counter == SHIFTREG_2_SIZE_BYTES) next_state = END_OF_LOAD_STB2;
			END_OF_LOAD_STB2: next_state = LOAD_TO_HEAD;
			LOAD_TO_HEAD: if (fifo_stb2_empty_delayed) next_state = DELAY_TO_WAIT_PRINT_LINE;
			DELAY_TO_WAIT_PRINT_LINE: next_state = WAIT_PRINT_LINE;
			WAIT_PRINT_LINE: next_state = WAIT_PRINT_LINE;
			LATCH: if (latch_clk == 2) next_state = IDLE;
		endcase
	end : state_logic


	always_ff @(posedge aclk)
		if (aresetn == '0)
			dummy_counter <= '0;
		else begin
			if (next_state == INIT)
				dummy_counter <= '0;
			if (next_state == LOAD_STB1_DUMMY)
				dummy_counter <= dummy_counter + 1'b1;
		end

	always_ff @(posedge aclk)
		if (aresetn == '0)
			valid_counter <= '0;
		else begin
			if ((next_state == INIT) || (next_state == END_OF_LOAD_STB1))
				valid_counter <= '0;

			if ((next_state == LOAD_STB1_VALID) || (next_state == LOAD_STB2_VALID))
				valid_counter <= valid_counter + 1'b1;
		end

	// Счетчик адреса BRAM
	always_ff @(posedge aclk)
		if (aresetn == '0) begin
			bram_address <= '0;
		end
		else begin
			if (next_state == INIT)
				bram_address <= '0;

			if ((next_state == LOAD_STB1_VALID) || (next_state == LOAD_STB2_VALID))
				bram_address <= bram_address + 1'b1;
		end

assign bram.addr = bram_address + 1'b1;
logic [7:0] bram_byte;
	mem32sel mem32sel_u(
		.data_in (bram.din),
		.sel     (bram_address[1:0]),
		.data_out(bram_byte)
	);


logic stb1_wr, stb2_wr;
logic [7:0] fifo_stb1_data, fifo_stb2_data;
	always_comb begin
		case (next_state)
			LOAD_STB1_DUMMY: begin
				stb1_wr <= 1'b1;
				fifo_stb1_data <= '0;
				stb2_wr <= 1'b0;
				fifo_stb2_data <= '0;
			end
			LOAD_STB1_VALID: begin
				stb1_wr <= 1'b1;
				fifo_stb1_data <= bram_byte;
				stb2_wr <= 1'b0;
				fifo_stb2_data <= '0;
			end
			LOAD_STB2_VALID: begin
				stb1_wr <= 1'b0;
				fifo_stb1_data <= '0;
				stb2_wr <= 1'b1;
				fifo_stb2_data <= bram_byte;
			end
			default: begin
				stb1_wr <= 1'b0;
				fifo_stb1_data <= '0;
				stb2_wr <= 1'b0;
				fifo_stb2_data <= '0;
			end
		endcase
	end



bit [3:0] clk_cnt;
bit head_clk;
bit head_latchn;

	always_ff @(posedge aclk)
		if (aresetn == '0) begin
			clk_cnt <= '0;
			head_clk <= '0;
		end
		else begin
			if (next_state != LOAD_TO_HEAD) begin
				clk_cnt <= '0;
				head_clk <= '0;
			end 
			else begin
				clk_cnt <= clk_cnt + 1'b1;
				if (clk_cnt[0] == 1'b1)
					head_clk <= ~head_clk;
			end
		end

	always_ff @(posedge aclk)
		head_latchn <= ~(next_state == LATCH);

	// Счетчик длины сигнала Latch
	always_ff @(posedge aclk)
		if (aresetn == '0)
			latch_clk <= '0;
		else
			if (next_state == LATCH)
				latch_clk <= latch_clk + 1'b1;

	assign data_loaded = (next_state == WAIT_PRINT_LINE);

bit [1:0] head_strobe;
assign HEAD_CLK = head_clk;
assign HEAD_LATCHn = head_latchn;
assign HEAD_STB = head_strobe;
	
	
	always_ff @(posedge aclk)
		if (aresetn == '0)
			head_strobe <= '0;
		else
			head_strobe <= '0;

	wire fifo_rd = clk_cnt[0]&clk_cnt[1];
	wire fifo_srst = (next_state == DELAY_TO_WAIT_PRINT_LINE);

	fifo8to1 fifo_stb1 (
		.clk(aclk),      // input wire clk
		.srst(fifo_srst),    // input wire srst
		.din(fifo_stb1_data),      // input wire [7 : 0] din
		.wr_en(stb1_wr),  // input wire wr_en
		.rd_en(fifo_rd),  // input wire rd_en
		.dout(HEAD_DO[0]),    // output wire [0 : 0] dout
		.full(),    // output wire full
		.empty(fifo_stb1_empty),  // output wire empty
		.rd_data_count(),  // output wire [9 : 0] rd_data_count
		.wr_data_count()  // output wire [6 : 0] wr_data_count
	);

	fifo8to1 fifo_stb2 (
		.clk(aclk),      // input wire clk
		.srst(fifo_srst),    // input wire srst
		.din(fifo_stb2_data),      // input wire [7 : 0] din
		.wr_en(stb2_wr),  // input wire wr_en
		.rd_en(fifo_rd),  // input wire rd_en
		.dout(HEAD_DO[1]),    // output wire [0 : 0] dout
		.full(),    // output wire full
		.empty(fifo_stb2_empty),  // output wire empty
		.rd_data_count(),  // output wire [9 : 0] rd_data_count
		.wr_data_count()  // output wire [6 : 0] wr_data_count
	);


endmodule


module mem32sel(
	input wire [31:0] data_in,
	input wire [1:0] sel,
	output reg [7:0] data_out
);
	always_comb begin
		case (sel)
			2'b00: data_out <= data_in[7:0];
			2'b01: data_out <= data_in[15:8];
			2'b10: data_out <= data_in[23:16];
			2'b11: data_out <= data_in[31:24];
		endcase
	end
endmodule
