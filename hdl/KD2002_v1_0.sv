`include "typedef.svh" 
	module KD2002_v1_0 #
	(
		// Users to add parameters here
        parameter integer C_BRAM_DATA_WIDTH = 32,
        parameter integer C_BRAM_ADDR_WIDTH = 32,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(


		// Users to add ports here
		input wire init,
		input wire print_line,
		output wire data_loaded,

		output wire HEAD_CLK,
		input wire [1:0] HEAD_DI,
		output wire [1:0] HEAD_DO,
		output wire [1:0] HEAD_STB,
		output wire HEAD_LATCHn,
		
        output wire [C_BRAM_ADDR_WIDTH-1:0] BRAM_PORT_addr,
        output wire BRAM_PORT_clk,
        output wire [C_BRAM_DATA_WIDTH-1:0] BRAM_PORT_dout,
        input wire [C_BRAM_DATA_WIDTH-1:0] BRAM_PORT_din,
        output wire BRAM_PORT_en,
        output wire BRAM_PORT_rst,
        output wire [C_BRAM_DATA_WIDTH/8-1:0] BRAM_PORT_we,
  
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
// Instantiation of Axi Bus Interface S00_AXI
	KD2002_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) KD2002_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
	);

	wire aclk = s00_axi_aclk;
	wire aresetn = s00_axi_aresetn;
    
    BRAM_if bramif (.clk(aclk));
    assign BRAM_PORT_clk = bramif.clk;
    assign BRAM_PORT_rst = bramif.rst;
    assign BRAM_PORT_we = bramif.we;
    assign BRAM_PORT_en = bramif.en;
    assign BRAM_PORT_dout = bramif.dout;
    assign BRAM_PORT_addr = bramif.addr;
    assign bramif.din = BRAM_PORT_din;

    KD2002 #(
	) KD2002_u (
		.aclk       (aclk),
		.aresetn    (aresetn),
		.HEAD_CLK   (HEAD_CLK),
		.HEAD_DI    (HEAD_DI),
		.HEAD_DO    (HEAD_DO),
		.HEAD_STB   (HEAD_STB),
		.HEAD_LATCHn(HEAD_LATCHn),
		.bram       (bramif),
		.init       (init),
		.print_line (print_line),
		.data_loaded(data_loaded)
	);

endmodule
