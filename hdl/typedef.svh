`ifndef AXI_TYPEDEF_SVH_
`define AXI_TYPEDEF_SVH_


interface BRAM_if #(ADDR_WIDTH=32, DATA_WIDTH=32)
	(input wire clk);

	logic rst;
	logic [ADDR_WIDTH-1:0] addr;
	logic [DATA_WIDTH-1:0] dout;
	logic [DATA_WIDTH-1:0] din;
	logic en;
	logic we;

	modport master (output rst, 
				 output addr, 
				 output dout, 
				 input din, 
				 output en, 
				 output we);
endinterface

`endif
